[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/carrot.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/carrot_15496882.htm#query=carrot&position=16&from_view=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "name": "Carrot",
  "type": "vegetable",
  "family": "Apiaceae",
  "synopsis": "It contains many minerals and vitamins. According to the saying, it would even make you likeable...",
  "preservation:": "8 days maximum in the refrigerator crisper",
  "nutritional": "40 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin B9",
  "history": "The inhabitants of the Mediterranean basin began to consume the carrot long before our era. But Greeks and Romans did not seem to appreciate it. At that time, carrots had a whitish color, a rather tough skin and a very fibrous heart. During the Renaissance, the species was improved and the carrot became tastier. But it is only from the middle of the 19th century that the carrot will acquire its beautiful orange-red color, and finally become the tender vegetable that we know.",
  "imagePath": "../../../assets/vegetables/icons/carrot.png",
  "cultureSheets": {
    "monthOfPlanting": [10, 11],
    "monthOfHarvest": [5, 11],
    "plantingInterval": null,
    "plantation": "Sow in October November. Transplant from March to July.",
    "harvest": "From May to November depending on the variety.",
    "soil": "Fresh and light, enriched the previous year, or the same year with vegetable compost.",
    "tasks": "Once a week, water with water and wood ashes. Ashes are rich in potassium, a mineral that carrots particularly appreciate.",
    "favourableAssociation": ["Leek", "Lettuce"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Lettuce", "Spinach", "Potato"],
    "unfavourablePrecedents": ["Bean"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/carotte) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 209 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 105

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
